<?php
/**
 * Created by PhpStorm.
 * User: cannibal
 * Date: 11.08.2017
 * Time: 13:41
 */

//User model class
class User
{
    protected $id;

    protected $email;

    protected $password;

    public $lastVisit;

    public $isLogedin;

    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getLastVisit()
    {
        return $this->lastVisit;
    }

    public function setLastVisit($lastVisit)
    {
        return $this->lastVisit = $lastVisit;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        return $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        return $this->id = $id;
    }
}