<?php

require "DBController.php";
require "User.php";
require "AutorizationController.php";
require "DBSetings.php";


$DB = new DBController;
$autorization = new AutorizationController();
$firstVisit = true;

$connection = $DB->connection($database, $dbUser, $dbPassword);


try {
    $DB->createTableIfNotExists('users', $connection);
} catch (PDOException $e) {
    die($e);
}


if (isset($_POST['email_reg'])) {
    $user = new User($_POST['email_reg'], $_POST['password_reg']);
} elseif (isset($_POST['email_sign'])) {
    $user = new User($_POST['email_sign'], $_POST['password_sign']);
} elseif ($autorization->isLogedin()) {
    $user = new User($_COOKIE['email'], 'nopassword');
    $user->setId($DB->userDetails($user, 'id', $connection));
    $user->setLastVisit($DB->userDetails($user, 'last_visit', $connection));
    $firstVisit = false;
}


if (isset($_POST['email_reg'])) {

    if ($DB->userDetails($user, 'email', $connection) == $_POST['email_reg']) {
        header("location:index.php?message=userAlreadyExist");
    }

    $DB->insertUser($user, $connection);
    $user->setId($DB->userDetails($user, 'id', $connection));
    $autorization->login($user);
    $firstVisit = false;

} elseif (isset($_POST['email_sign'])) {
    $user->setId($DB->userDetails($user, 'id', $connection));
    $user->setLastVisit($DB->userDetails($user, 'last_visit', $connection));

    if (!$autorization->checkPassword($user, $DB->userDetails($user, 'password', $connection))) {
        $tries = $DB->userDetails($user, 'tries', $connection);
        $tries++;

        if ($tries == 3) {
            $autorization->blockUser($user);
            $tries = 0;
            $DB->updateUser($user, 'tries', $tries, 'int', $connection);
            $DB->updateUser($user, 'blocktime', time(), 'int', $connection);
            header("location:index.php?message=isBlocked");
            exit();
        } else {
            $DB->updateUser($user, 'tries', $tries, 'int', $connection);

            header("location:index.php?message=invalid_password");
            exit();
        }


    };

    if (isset($_COOKIE['isBlocked']) || (time() - $DB->userDetails($user, 'blocktime', $connection)) < 120) {
        header("location:index.php?message=isBlocked");
        exit();
    }

    $DB->updateUser($user, 'last_visit', date('d.m.Y'), 'str', $connection);

    $autorization->login($user);
    $firstVisit = false;
}


require "index.view.php";


