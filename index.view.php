<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Solutions</title>

    <!-- Bootstrap core CSS -->
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <table>
        <tr>
            <td>
                <form class="form-signin" method="post" action="/">
                    <h2 class="form-signin-heading">Зареєструйтесь</h2>
                    <label for="inputEmail" class="sr-only">Email адреса</label>
                    <input name="email_reg" type="email" id="inputEmail" class="form-control" placeholder="Email адреса"
                           required autofocus>
                    <label for="inputPassword" class="sr-only">Пароль</label>
                    <input name="password_reg" type="password" id="inputPassword" class="form-control"
                           placeholder="Пароль" required>
                    <div class="checkbox">
                        <label>
                            <input name="rememberme" type="checkbox" value="true"> Запам'ятайте мене
                        </label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Увійти</button>
                </form>
            </td>
            <td width="200px" align="center"><b>АБО</b></td>
            <td>
                <form class="form-signin" id="signin" method="post" action="/">
                    <h2 class="form-signin-heading">Увійдіть до системи</h2>
                    <label for="inputEmail" class="sr-only">Email адреса</label>
                    <input name="email_sign" type="email" id="inputEmail" class="form-control"
                           placeholder="Email адреса" required autofocus>
                    <label for="inputPassword" class="sr-only">Пароль</label>
                    <input name="password_sign" type="password" id="inputPassword" class="form-control"
                           placeholder="Пароль" required>
                    <div class="checkbox">
                        <label>
                            <input name="rememberme" type="checkbox" value="true"> Запам'ятайте мене
                        </label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Увійти</button>
                </form>
            </td>
        </tr>
    </table>

    <hr>

    <?php if (isset($_GET['message'])) {
        if ($_GET['message'] == 'isBlocked') {
            echo '<b>Вас заблоковано на 2хв</b>';
        } elseif ($_GET['message'] == 'invalid_password') {
            echo '<b>Пароль не вірний</b>';
        } elseif ($_GET['message'] == 'userAlreadyExist') {
            echo '<b>Користувач з таким Email вже існує</b>';
        }
    } else {
        if ($firstVisit == false) {
            require "login_info.html";
        }

    } ?>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
