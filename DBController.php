<?php


class DBController
{

    public function connection($database, $user, $password)
    {
        return new PDO($database, $user, $password);
    }

    public function createTableIfNotExists($table, $connection)
    {
        $statementCreateTable = $connection->prepare('
                CREATE TABLE IF NOT EXISTS ' . $table . ' (
                ID int PRIMARY KEY AUTO_INCREMENT,
                email varchar(255) NOT NULL UNIQUE,
                password varchar(255) NOT NULL,
                last_visit varchar (10),
                blocktime int(10),
                tries INT(1) NOT NULL DEFAULT "' . 0 . '");
        ');
        return $statementCreateTable->execute();
    }

    public function insertUser(User $user, $connection)
    {
        $statementIns = $connection->prepare('insert into users (email,password) value ("' . $user->getEmail() . '","' . $user->getPassword() . '")');
        return $statementIns->execute();
    }

    public function updateUser(User $user, $field, $value, $valueType, $connection)
    {
        if ($valueType == 'str') {
            $statementUpd = $connection->prepare('update users set ' . $field . '="' . $value . '" where email="' . $user->getEmail() . '" ');
        } elseif ($valueType == 'int') {
            $statementUpd = $connection->prepare('update users set ' . $field . '=' . $value . ' where email="' . $user->getEmail() . '" ');
        }
        return $statementUpd->execute();

    }

    public function userDetails(User $user, $field, $connection)
    {
        $statement = $connection->prepare('select ' . $field . ' from users where email="' . $user->getEmail() . '"');
        $statement->execute();
        $userDetails = $statement->fetchAll();
        return $userDetails['0'][$field];
    }
}





