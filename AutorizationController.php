<?php

class AutorizationController
{

    public function login(User $user)
    {
        if (isset($_POST['rememberme'])) {

            setcookie('isLogedin', $user->getEmail(), time() + 86400 * 30);
            setcookie('email', $user->getEmail(), time() + 86400 * 30);
            $user->isLogedin = 1;

        }
        return $user;

    }

    public function logout(User $user)
    {
        setcookie('isLogedin', $user->getEmail(), time() - 3600);
        setcookie('email', $user->getEmail(), time() - 3600);
        $user->isLogedin = 0;
        return $user;
    }


    public function blockUser(User $user)
    {
        setcookie('isBlocked', $user->getEmail(), time() + 180);
        return $user;
    }

    public function unBlockUser(User $user)
    {
        setcookie('isBlocked', $user->getEmail(), time() - 3600);
        return $user;
    }

    public function checkPassword(User $user, $password)
    {
        if ($user->getPassword() == $password) {
            return true;
        }
        return false;
    }

    public function isLogedin()
    {
        if (isset($_COOKIE['isLogedin'])) {
            return true;
        }
        return false;
    }

}